# Presentations

* [A la chasse aux faux semblants](http://l.mazardo.free.fr/prez_A-la-chasse-aux-faux-semblants) - [source](https://github.com/lmazardo/A-la-chasse-aux-faux-semblants)
* [Développement de rôles ansible guidé les tests](https://lucpasc.gitlab.io/presentation/) 
* [Devoops](https://xtrmz.gitlab.io/devoops/index.html)

## Support formations

* [Sensibilisation agile](https://github.com/lmazardo/sensibilisation_agile) - cours IUT
* https://gitlab.com/xtrmz/les_enfants_et_les_ecrans
